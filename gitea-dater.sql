UPDATE
  repository
set
  updated_unix = sub.commit_time
FROM
  (
    SELECT
      r.id,
      max(b.commit_time) as commit_time
    FROM
      repository AS r
      INNER JOIN branch b ON b.repo_id = r.id
    where
      r.is_archived = true
    GROUP BY
      r.id
  ) as sub
where
  repository.id = sub.id;
