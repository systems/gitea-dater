# Gitea dater

Set the updates dates for Gitea repositories.

See also [`gitlab-dater`](https://git.theorangeone.net/systems/gitlab-dater).

## Usage

Note: By default, only archived repositories are included.

`date-audit.sql` will list the repositories, their current archived date and the date the repository itself was last updated.

`gitea-dater.sql` will set the updated dates to the latest commit date on the repository itself.
