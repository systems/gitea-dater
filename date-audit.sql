SELECT
  r.owner_name,
  r.lower_name,
  to_timestamp(
    max(b.commit_time)
  ),
  to_timestamp(r.updated_unix)
FROM
  repository AS r
  INNER JOIN branch b ON b.repo_id = r.id
where
  r.is_archived = true
GROUP BY
  r.owner_name,
  r.lower_name,
  r.updated_unix
ORDER BY
  r.owner_name,
  r.lower_name;
